import com.google.common.base.Verify;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.IOException;


public class SFPageTest extends BaseTest{

    @Test
    public void truePageTitle() {
        MainPage mainPage = new MainPage();
        String userLoginTitle = mainPage.getPageTitle();
        Assert.assertEquals(userLoginTitle, "ToolsQA");
    }

    @Test
    public void navigateElements() {
        MainPage mainPage = new MainPage();
        String mainHeaderText = mainPage.clickToElements("Elements").headerText();
        String url = mainPage.getUrl();
        System.out.println(url);
        Assert.assertEquals(mainHeaderText, "Elements");
    }

    @Test
    public void navigateForms() {
        MainPage mainPage = new MainPage();
        String mainHeaderText = mainPage.clickToElements("Forms").headerText();
        Assert.assertEquals(mainHeaderText, "Forms");
    }

    @Test
    public void navigateAlerts() {
        MainPage mainPage = new MainPage();
        String elemTitle = mainPage.clickToElements("Alerts, Frame & Windows").headerText();
        Assert.assertEquals(elemTitle, "Alerts, Frame & Windows");
    }

    @Test
    public void navigateTextBox() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Elements");
        ElementsPage elementsPage = new ElementsPage();
        String headerText = elementsPage.clickToBox().getMainHeaderText();
        Assert.assertEquals(headerText, "Text Box");
    }

    @Test
    public void submitTextBox() throws IOException {
        PropertyValues prop = new PropertyValues();
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Elements");
        ElementsPage elementsPage = new ElementsPage();
        elementsPage.clickToBox();
        TextBoxPage textBoxPage = new TextBoxPage();
        textBoxPage.
                fillTextBoxForm(prop.getPropertyValue("fullName"),
                        prop.getPropertyValue("email"),
                        prop.getPropertyValue("currentAddress"),
                        prop.getPropertyValue("permanentAddress"));
        String result = textBoxPage.resultTextBox();
        Assert.assertEquals(result, "Name:" + prop.getPropertyValue("fullName"));
    }

    @Test
    public void pressCheckBox() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Elements");
        ElementsPage elementsPage = new ElementsPage();
        elementsPage.clickToCheckBox();
        CheckBoxPage checkBoxPage = new CheckBoxPage();
        checkBoxPage.pressCheckBox();
        String result = checkBoxPage.resultCheckBox();
        Assert.assertTrue(result.contains("You have selected :"));
    }

    @Test
    public void pressYesRadioButton() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Elements");
        ElementsPage elementsPage = new ElementsPage();
        elementsPage.clickToRadioButton();
        RadioButtonPage radioButtonPage = new RadioButtonPage();
        radioButtonPage.selectRadioButton("Yes");
        String result = radioButtonPage.resultRadioButton();
        Assert.assertEquals(result, "Yes");
    }

    @Test
    public void pressImpressiveRadioButton() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Elements");
        ElementsPage elementsPage = new ElementsPage();
        elementsPage.clickToRadioButton();
        RadioButtonPage radioButtonPage = new RadioButtonPage();
        radioButtonPage.selectRadioButton("Impressive");
        String result = radioButtonPage.resultRadioButton();
        Assert.assertEquals(result, "Impressive");
    }

    @Test
    public void pressNoRadioButton() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Elements");
        ElementsPage elementsPage = new ElementsPage();
        elementsPage.clickToRadioButton();
        RadioButtonPage radioButtonPage = new RadioButtonPage();
        radioButtonPage.disabledRadioButton();
        String result = radioButtonPage.headerText();
        Assert.assertEquals(result,"Radio Button");
    }

    @Test
    public void navigateToPracticeForms() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Forms");
        Forms forms = new Forms();
        forms.clickToPracticeForm();
        PracticeFormPage practiceFormPage = new PracticeFormPage();
        String text = practiceFormPage.practiceFormMainText();
        Assert.assertEquals(text, "Student Registration Form");
    }

    @Test (alwaysRun = true)
    public void sendForm() throws IOException {
        PropertyValues prop = new PropertyValues();
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Forms");
        Forms forms = new Forms();
        forms.clickToPracticeForm();
        PracticeFormPage practiceFormPage = new PracticeFormPage();
        practiceFormPage.fillPracticeForm(prop.getPropertyValue("name"), prop.getPropertyValue("lastName"), prop.getPropertyValue("mobile"));
        String text = practiceFormPage.getValueFromCell(4,"Values");
        System.out.println(text);
        String testResult = practiceFormPage.checkStage();
        Assert.assertEquals(testResult, "Haryana");

    }

/*    @Test
    public void helpSalesforce() throws IOException, InterruptedException {
        PropertyValues prop = new PropertyValues();
        WebDriverManager.getDriver().get(prop.getPropertyValue("baseurlSF"));
        HelpSalesforce helpSalesforce = new HelpSalesforce();
        String title = helpSalesforce.getTitle();
        Assert.assertEquals(title, "Help | Training | Salesforce");
        helpSalesforce.findText(prop.getPropertyValue("request"));
        SearchPage searchPage = new SearchPage();
        String langTitle = searchPage.getLanguageTitleText();
        System.out.println(langTitle);
        Assert.assertEquals(langTitle, "LANGUAGE");
        searchPage.expandLanguageBlock().selectNewLanguage("Русский");
        searchPage.selectNewLanguage("Deutsch");
        searchPage.waitForLoadingSpinner();
        WebDriverWait waitSearchResult = new WebDriverWait(searchPage.driver, 5);
        waitSearchResult.until(ExpectedConditions.invisibilityOfElementLocated(Locators.SearchPageLoc.languageSpinner));
        Actions actions = new Actions(searchPage.driver);
        WebElement list = Page.getElement(Locators.SearchPageLoc.listResults);
        actions.moveToElement(list).build().perform();
        searchPage.clickFirstResult();
        SalesforceForOutlook salesforceForOutlook = new SalesforceForOutlook();
        waitSearchResult.until(ExpectedConditions.presenceOfElementLocated(Locators.SalesforceForOutlook.sldsSpinner));
        waitSearchResult.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@id='topic-title']")));
        String salesforceForOutlookTitle = salesforceForOutlook.checkTitle();
        Assert.assertEquals(salesforceForOutlookTitle,"Salesforce for Outlook");
    }
*/
    @Test
    public void alerts() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Alerts, Frame & Windows");
        Alerts alert = new Alerts();
        alert.clickToLeftSubNav("Alerts");
        AlertsPage alertsPage = new AlertsPage();
        alertsPage.clickButton("alertButton");
        try {
            Thread.sleep(1500);
            } catch (InterruptedException e) {
            e.printStackTrace();
        }
        alertsPage.acceptAlert();
    }

    @Test
    public void timerAlertButton() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Alerts, Frame & Windows");
        Alerts alert = new Alerts();
        alert.clickToLeftSubNav("Alerts");
        AlertsPage alertsPage = new AlertsPage();
        alertsPage.clickButton("timerAlertButton");
        alertsPage.waitAlert();
        alertsPage.dismissAlert();
    }

    @Test
    public void confirmAlert() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Alerts, Frame & Windows");
        Alerts alert = new Alerts();
        alert.clickToLeftSubNav("Alerts");
        AlertsPage alertsPage = new AlertsPage();
        alertsPage.clickButton("confirmButton");
        alertsPage.waitAlert();
        alertsPage.acceptAlert();
        String response = alertsPage.confirmResult();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response,"You selected Ok");
    }

    @Test
    public void fillAlert() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Alerts, Frame & Windows");
        Alerts alert = new Alerts();
        alert.clickToLeftSubNav("Alerts");
        AlertsPage alertsPage = new AlertsPage();
        alertsPage.clickButton("promtButton");
        alertsPage.waitAlert();
        alertsPage.fillAlert("Text");
        alertsPage.acceptAlert();
        String response = alertsPage.promptResult();
        Assert.assertEquals(response,"You entered Text");
    }
    @Test
    public void newTab() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Alerts, Frame & Windows");
        Alerts alert = new Alerts();
        alert.clickToLeftSubNav("Browser Windows");
        AlertsPage browserWindows = new AlertsPage();
        browserWindows.clickButton("tabButton");
        browserWindows.goToNewTab();
        Verify.verify(browserWindows.mainText().equals("This is a sample page"),"The text doesn't match");

    }

    @Test
    public void newWindow() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Alerts, Frame & Windows");
        Alerts alert = new Alerts();
        alert.clickToLeftSubNav("Browser Windows");
        AlertsPage browserWindows = new AlertsPage();
        browserWindows.clickButton("windowButton");
        browserWindows.goToNewWindow();
        String result = browserWindows.mainText();
        Assert.assertEquals("This is a sample page",result);

    }

    @Test
    public void checkIframe () {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Alerts, Frame & Windows");
        Alerts alert = new Alerts();
        alert.clickToLeftSubNav("Frames");
        Frames frame = new Frames();
        frame.getIframe("frame1");
        frame.getMainText();
        frame.getIframe("frame2");
        frame.getMainText();
    }

    @Test
    public void checkNestedIframes () {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Alerts, Frame & Windows");
        Alerts alert = new Alerts();
        alert.clickToLeftSubNav("Nested Frames");
        Frames frame = new Frames();
        frame.getParentFrame();
        frame.getChildFrame();
        String text = frame.getChildText();
        Verify.verify(text.equals("Child Iframe"), "The text doesn't match ");
        Page.driver.navigate().back();
    }

    @Test
    public void smallModal() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Alerts, Frame & Windows");
        Alerts alert = new Alerts();
        alert.clickToLeftSubNav("Modal Dialogs");
        AlertsPage modal = new AlertsPage();
        modal.clickButton("showSmallModal");
        String textResult = modal.GetTextInSmallModal();
        Assert.assertEquals(textResult, "This is a small modal. It has very less content");
        modal.clickButton("closeSmallModal");
    }

    @Test void largeModal() {
        MainPage mainPage = new MainPage();
        mainPage.clickToElements("Alerts, Frame & Windows");
        Alerts alert = new Alerts();
        alert.clickToLeftSubNav("Modal Dialogs");
        AlertsPage modal = new AlertsPage();
        modal.clickButton("showLargeModal");
        String textResult = modal.GetTextInLargeModal();
        Assert.assertEquals(textResult, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        modal.clickButton("closeLargeModal");
    }
}
