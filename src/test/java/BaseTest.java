import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    @BeforeMethod
    public static void setUp() throws IOException {
        WebDriverManager.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverManager.getDriver().manage().window().maximize();
        PropertyValues propertyValues = new PropertyValues();
        WebDriverManager.getDriver().get(propertyValues.getPropertyValue("baseurl"));
        System.out.println("Successfully followed the link");
    }
    @AfterMethod
    public void tearDown() {
        Date dateNow = new Date();
        SimpleDateFormat format = new SimpleDateFormat("hh_mm_ss");
        String fileName = format.format(dateNow) + ".png";
        File screenshot = ((TakesScreenshot) WebDriverManager.getDriver()).
                getScreenshotAs(OutputType.FILE);

        try {
            FileUtils.copyFile(screenshot, new File(System.getProperty("user.dir")+ "./target/screenshots/test" + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }


        WebDriverManager.stopDriver();
        System.out.println("The close_up process is completed");
    }
}
