import org.openqa.selenium.By;

public class Alerts extends Page{
    public Alerts() {
        super();
    }
    public AlertsPage clickToLeftSubNav (String subMenu) {
        getElement(By.xpath(String.format(Locators.AlertsFrameWindows.subNav, subMenu))).click();
        return new AlertsPage();
    }


}
