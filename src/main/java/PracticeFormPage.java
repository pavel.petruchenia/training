import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PracticeFormPage extends Page{
    public PracticeFormPage() {
        super();
    }
    public String practiceFormMainText() {
        return getElement(Locators.PracticeFormPageElements.mainText).getText();
    }
    public void selectRadio(String nameRadioButton) {
        if (!getElement(By.xpath(String.format(Locators.PracticeFormPageElements.radioButtonXpath,nameRadioButton))).isSelected())
            getElement(By.xpath(String.format(Locators.RadioButtonPageElements.rbXpath,nameRadioButton))).click();
    }
    public PracticeFormPage enterFirstName(String name) {
        getElement(Locators.PracticeFormPageElements.name).sendKeys(name);
        return this;
    }
    public PracticeFormPage enterLastName(String lastName) {
        getElement(Locators.PracticeFormPageElements.lastName).sendKeys(lastName);
        return this;
    }
    public PracticeFormPage enterMobile(String mobile) {
        getElement(Locators.PracticeFormPageElements.mobile).sendKeys(mobile);
        return this;
    }

    public PracticeFormPage chooseState(String selectState, String selectCity) {
        if (!getElement(By.xpath(String.format(Locators.PracticeFormPageElements.state))).isSelected())
            getElement(By.xpath(String.format(Locators.PracticeFormPageElements.state))).click();
        getElement(By.xpath(String.format(Locators.PracticeFormPageElements.selectState,selectState))).click();
        getElement(By.xpath(String.format(Locators.PracticeFormPageElements.city))).click();
        getElement(By.xpath(String.format(Locators.PracticeFormPageElements.selectCity,selectCity))).click();
        return this;
    }

    public PracticeFormPage pressSubmitButton() {
        getElement(Locators.PracticeFormPageElements.submitButton).click();
        return this;
    }
    public String checkStage() {
        return getElement(Locators.PracticeFormPageElements.stateResult).getText();
    }

    public PracticeFormPage fillPracticeForm(String name, String lastName, String mobile) {
        this.enterFirstName(name);
        this.enterLastName(lastName);
        this.enterMobile(mobile);
        this.selectRadio("Female");
        this.chooseState("Haryana", "Karnal");
        this.pressSubmitButton();
        return new PracticeFormPage();
    }
    public List<WebElement> getRows(){

        List<WebElement> rows = driver.findElements(Locators.PracticeFormPageElements.tableRows);
        return rows;
    }

    public List<WebElement> getHeadings() {
        WebElement headingsRow = getElement(By.xpath(".//thead/tr"));
        List<WebElement> headingColums = headingsRow.findElements(By.xpath(".//th"));
        return headingColums;
    }

    public List<List<WebElement>> getRowsWithColumns() {
        List<WebElement> rows = getRows();
        List<List<WebElement>> rowsWithColumns = new ArrayList<List<WebElement>>();
        for (WebElement row : rows) {
            List<WebElement> rowWithColumns = row.findElements(By.xpath(".//td"));
            rowsWithColumns.add(rowWithColumns);
        }
        return rowsWithColumns;
    }

    public List<Map<String, WebElement>> getRowsWithColumnByHeading() {
        List<List<WebElement>> rowsWithColumns = getRowsWithColumns();
        List<Map<String, WebElement>> rowsWithColumnByHeading = new ArrayList<Map<String, WebElement>>();
        Map<String, WebElement> rowByHeadings;
        List<WebElement> headingColumns = getHeadings();

        for (List<WebElement> row : rowsWithColumns) {
            rowByHeadings = new HashMap<String, WebElement>();
            for (int i = 0; i < headingColumns.size(); i++) {
                String heading = headingColumns.get(i).getText();
                WebElement cell = row.get(i);
                rowByHeadings.put(heading,cell);
            }
            rowsWithColumnByHeading.add(rowByHeadings);
        }
        return rowsWithColumnByHeading;
    }

    public String getValueFromCell (int rowNumber, String columnName) {
        List<Map<String, WebElement>> rowsWithColumnsByHeading = getRowsWithColumnByHeading();
        return rowsWithColumnsByHeading.get(rowNumber-1).get(columnName).getText();
    }


}
