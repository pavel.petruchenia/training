import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Locators {
    public static class MainPageLocators {
        public static final String elements = "//h5[contains(text(),'%s')]/parent::div/preceding-sibling::div[@class='avatar mx-auto white']";
    }
    public static class PageElements {
        public static final By textHeader = By.xpath("//div[@class='main-header']");
        public static final By textBoxPageLink = By.xpath("//span[@class='text'][text()='Text Box']");
        public static final By checkBoxPageLink = By.xpath("//span[@class='text'][text()='Check Box']");
        public static final By radioButtonPageLink = By.xpath("//span[@class='text'][text()='Radio Button']");

    }
    public static class TextBoxPageElements {
        public static final By fullName = By.xpath("//input[@id='userName']");
        public static final By actualResult = By.xpath("//p[@id='name']");
        public static final By email = By.xpath("//input[@id='userEmail']");
        public static final By currentAddress = By.xpath("//textarea[@id='currentAddress']");
        public static final By permanentAddress = By.xpath("//textarea[@id='permanentAddress']");
        public static final By submitButton = By.xpath("//button[@id='submit']");
    }
    public static class CheckBoxPageElements {
        public static final By checkBox = By.xpath("//span[@class='rct-checkbox']");
        public static final By checkBoxResult = By.xpath("//div[@id='result']");
    }
    public static class RadioButtonPageElements {
        public static final By noRadioButton = By.xpath("//label[@for='noRadio']");
        public static final By radioButtonResult = By.xpath("//span[@class='text-success']");
        public static final String rbXpath = "//label[text()='%s']";
    }
    public static class FormsPageElements {
        public static final By practiceFormPageLink = By.xpath("//span[@class='text'][text()='Practice Form']");
    }
    public static class PracticeFormPageElements {
        public static final By name = By.xpath("//input[@id='firstName']");
        public static final By lastName = By.xpath("//input[@id='lastName']");
        public static final By mobile = By.xpath("//input[@id='userNumber']");
        public static final String selectState = ("//div[text()='%s']");
        public static final String state = ("//div[@id='state']");
        public static final String selectCity = ("//div[text()='%s']");
        public static final By stateResult = By.xpath ("//div[text()='Haryana']");
        public static final String city = ("//div[@id='city']");
        public static final By submitButton = By.xpath("//button[@id='submit']");
        public static final By mainText = By.xpath("//h5");
        public static final String radioButtonXpath = "//label[text()='%s']";
        public static final By tableElement = By.xpath("//table");
        public static final By tableRows = By.xpath(".//tbody/tr");
    }
    public static class HelpSF {
        public static final By search = By.xpath("//input[@aria-label='Search']");
    }
    public static class SearchPageLoc {
        public static final By languageTitle = By.xpath("//div[@class='coveo-facet-header-title'][text()='Language']");
        public static final By language = By.xpath("//*[@id='LanguageFacet']/ul[@class='coveo-facet-values']/li");
        public static final By openLanguageList = By.xpath("//*[@id='LanguageFacet']//span[@class='coveo-facet-more-icon']");
        public static final By searchResult= By.xpath("//div[@class='CoveoResultList']//a");
        public static final By languageSpinner = By.xpath("//div[@class='coveo-facet-header']//div[text()='Language']/following-sibling::div[@class='coveo-facet-header-wait-animation'][@style='visibility: visible;']");
        public static final String checkBox= "//ul[@class='coveo-facet-values']//span[@title='%s']";
        public static final By listResults = By.xpath("//span[@class='CoveoQuerySummary']");
    }
    public static class SalesforceForOutlook {
        public static final By sldsSpinner = By.xpath("//div[@class='slds-spinner_container']");
    }
    public static class AlertsFrameWindows {
        public static final String subNav = "//span[@class='text'][text()='%s']";
        public static final String alertButton = "//button[@id='%s']";
        public static final By result = By.xpath("//span[@id='confirmResult']");
        public static final By promptResult = By.xpath("//span[@id='promptResult']");
        public static final By title = By.xpath("//h1[@id='sampleHeading']");
        public static final String iframe = "//*[@id='%s']";
        public static final By iframeMainText = By.xpath("/html/body/text()");
        public static final By childFrame = By.xpath("//*[@srcdoc='<p>Child Iframe</p>']");
        public static final By childFrameText = By.xpath("/html/body/p");
        public static final By ParentText = By.xpath("//iframe[@id='frame1']//body");
        public static final By parentFrame = By.xpath("//iframe[@id='frame1']");
        public static final By smallModalText = By.xpath("//div[@class='modal-body']");
        public static final By largeModalText = By.xpath("//p/parent::div[@class='modal-body']");
    }
}
