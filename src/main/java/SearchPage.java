import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SearchPage extends Page {

    public SearchPage() {
            super();
        }
    public String getLanguageTitleText() {
        Wait<WebDriver> waitFluent = new FluentWait<WebDriver>(driver).withTimeout(15, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchFieldException.class);
        waitFluent.until(ExpectedConditions.visibilityOfElementLocated(Locators.SearchPageLoc.languageTitle));
        return driver.findElement(Locators.SearchPageLoc.languageTitle).getText();
    }

    public SearchPage expandLanguageBlock() {
        if (driver.findElements(Locators.SearchPageLoc.language).size() < 7)
            System.out.println("открываем блок с языками");
            System.out.println(driver.findElements(Locators.SearchPageLoc.language).size());
            getElement(Locators.SearchPageLoc.openLanguageList).click();
            System.out.println("Блок с языками открыт");
        return this;
    }

    public void selectNewLanguage(String nameCheckBox) {
        if (!getElement(By.xpath(String.format(Locators.SearchPageLoc.checkBox, nameCheckBox))).isSelected()) {
            getElement(By.xpath(String.format(Locators.SearchPageLoc.checkBox, nameCheckBox))).click();
            System.out.println("The checkbox has been checked " + nameCheckBox + " language");
        } else System.out.println("The " + nameCheckBox + " language already selected");
    }

    public void unSelectLanguage(String nameCheckBox) {
        if (getElement(By.xpath(String.format(Locators.SearchPageLoc.checkBox, nameCheckBox))).isSelected()) {
            getElement(By.xpath(String.format(Locators.SearchPageLoc.checkBox, nameCheckBox))).click();
            System.out.println("The checkbox has been unchecked " + nameCheckBox + " language");
        } else if (getElement(By.xpath(String.format(Locators.SearchPageLoc.checkBox, nameCheckBox))).isSelected())
            System.out.println("The checkbox has not been unchecked");
    }

    public void waitForLoadingSpinner() {
        getElement(Locators.SearchPageLoc.languageSpinner);
    }

    public SalesforceForOutlook clickFirstResult() {
        List<WebElement> listElements = driver.findElements(Locators.SearchPageLoc.searchResult);
        Actions actions = new Actions(driver);
        WebElement list = getElement(Locators.SearchPageLoc.listResults);
        actions.moveToElement(list).build().perform();
        listElements.get(0).click();
        return new SalesforceForOutlook();
    }




}
