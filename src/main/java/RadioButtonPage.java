import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class RadioButtonPage extends Page{
    public RadioButtonPage() {
        super();
    }
    public void selectRadioButton(String nameRadioButton) {
       if (!getElement(By.xpath(String.format(Locators.RadioButtonPageElements.rbXpath,nameRadioButton))).isSelected())
           getElement(By.xpath(String.format(Locators.RadioButtonPageElements.rbXpath,nameRadioButton))).click();
    }

    public void disabledRadioButton() {
        WebElement noRadiobutton = getElement(Locators.RadioButtonPageElements.noRadioButton);
        if (noRadiobutton.isEnabled()) {
            System.out.println("Locator is enabled");
            noRadiobutton.click();
            if(noRadiobutton.isSelected())
            {
                System.out.println("Radio is selected");
            }
            System.out.println("Radio button is disabled");
        } else System.out.println("Locator is disabled");

    }
    public String resultRadioButton() {
        return getElement(Locators.RadioButtonPageElements.radioButtonResult).getText();
    }
    public String headerText() {
        return getElement(Locators.PageElements.textHeader).getText();
    }
}
