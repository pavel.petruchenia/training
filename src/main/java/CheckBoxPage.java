import org.openqa.selenium.By;

public class CheckBoxPage extends Page  {
    public CheckBoxPage() {
        super();
    }

    public void pressCheckBox() {
        getElement(Locators.CheckBoxPageElements.checkBox).click();
    }
    public String resultCheckBox() {
        return getElement(Locators.CheckBoxPageElements.checkBoxResult).getText();
    }
}
