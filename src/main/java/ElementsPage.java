public class ElementsPage extends Page {

    public ElementsPage() {
        super();
    }

    public String headerText() {
        return getElement(Locators.PageElements.textHeader).getText();
    }

    public TextBoxPage clickToBox() {
        getElement(Locators.PageElements.textBoxPageLink).click();
        return new TextBoxPage();
    }
    public CheckBoxPage clickToCheckBox() {
        getElement(Locators.PageElements.checkBoxPageLink).click();
        return new CheckBoxPage();
    }
    public CheckBoxPage clickToRadioButton() {
        getElement(Locators.PageElements.radioButtonPageLink).click();
        return new CheckBoxPage();
    }
}
