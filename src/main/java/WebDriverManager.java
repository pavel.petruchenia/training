import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.By;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class WebDriverManager {
    private static WebDriver driver;

    public static WebDriver getDriver() {
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
            System.out.println("Start driver");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--headless");
            options.addArguments("--disable-gpu");
            options.addArguments("--disable-web-security");
            options.addArguments("--window-size=1920,1080");
            options.addArguments("--start-maximized");
            driver = new ChromeDriver(options);
            return driver;
        }
        else return driver;
    }
    public static void stopDriver() {
        driver.quit();
        driver = null;
    }

}
