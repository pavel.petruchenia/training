import org.openqa.selenium.By;

import java.util.List;

public class MainPage extends Page {

    public MainPage() {
        super();
    }

    public ElementsPage clickToElements(String elementName) {
        getElement(By.xpath(String.format(Locators.MainPageLocators.elements, elementName))).click();
        return new ElementsPage();
    }

    public String getUrl() {
        return driver.getCurrentUrl();
    }
}
