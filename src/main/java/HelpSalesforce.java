import org.openqa.selenium.Keys;

public class HelpSalesforce extends Page {
    public HelpSalesforce() {
        super();
    }
    public String getTitle() {
        return driver.getTitle();
    }
    public HelpSalesforce findText(String request) {
        getElement(Locators.HelpSF.search).sendKeys(request);
        getElement(Locators.HelpSF.search).sendKeys(Keys.ENTER);
        return  this;
    }
}
