public class Forms extends Page{
    public Forms() {
        super();
    }
    public PracticeFormPage clickToPracticeForm() {
        getElement(Locators.FormsPageElements.practiceFormPageLink).click();
        return new PracticeFormPage();
    }
}
