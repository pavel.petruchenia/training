import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.Format;
import java.util.List;

public class AlertsPage extends Page {
    public AlertsPage() {
        super();
    }

    public void clickButton(String buttonName) {
        getElement(By.xpath(String.format(Locators.AlertsFrameWindows.alertButton, buttonName))).click();

    }
    public void acceptAlert() {
        driver.switchTo().alert().accept();
    }

    public void dismissAlert() {
        driver.switchTo().alert().dismiss();
    }
    public void waitAlert() {
        WebDriverWait waitAlert = new WebDriverWait(driver, 15);
        waitAlert.until(ExpectedConditions.alertIsPresent());
    }
    public String confirmResult() {
        return getElement(Locators.AlertsFrameWindows.result).getText();
    }
    public String promptResult() {
        return getElement(Locators.AlertsFrameWindows.promptResult).getText();
    }

    public void fillAlert(String text) {
        driver.switchTo().alert().sendKeys(text);
    }
    public void goToNewTab() {
        for (String tab : driver.getWindowHandles()) {
            driver.switchTo().window(tab);
        }
    }

    public void goToNewWindow() {
        for (String window : driver.getWindowHandles()) {
            driver.switchTo().window(window);

        }
    }
    public void closeCurrentPage() {
        driver.close();
    }
    public String mainText() {
        return getElement(Locators.AlertsFrameWindows.title).getText();
    }

    public String GetTextInSmallModal() {
        return getElement(Locators.AlertsFrameWindows.smallModalText).getText();
    }

    public String GetTextInLargeModal() {
        return getElement(Locators.AlertsFrameWindows.largeModalText).getText();
    }

}
