import org.openqa.selenium.By;

public class Frames extends Page {
    public Frames() {
        super();
    }

    public void getIframe(String number) {
        driver.switchTo().defaultContent();//
        driver.switchTo().frame(getElement(By.xpath(String.format(Locators.AlertsFrameWindows.iframe, number))));
    }
    public String getMainText() {
        return getElement(Locators.AlertsFrameWindows.title).getText();
    }
    public void getChildFrame () {
        driver.switchTo().frame(getElement(Locators.AlertsFrameWindows.childFrame));
    }
    public void getParentFrame () {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(getElement(Locators.AlertsFrameWindows.parentFrame));
    }
    public String getChildText() {
        return getElement(Locators.AlertsFrameWindows.childFrameText).getText();
    }
    public String getParentText() {
        return getElement(Locators.AlertsFrameWindows.ParentText).getText();

}}
