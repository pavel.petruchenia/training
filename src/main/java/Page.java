import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class Page {
    public static WebDriver driver;

    public Page() {
    this.driver = WebDriverManager.getDriver();

    }
    public static WebElement getElement(By bySomething) {
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(30, TimeUnit.SECONDS)
                .pollingEvery(5, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .withMessage("Element was not found");

        WebElement fluentWait = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(bySomething);
            }
        });
        return fluentWait;
    }
    public String getPageTitle() {
        return driver.getTitle();
    }
}
