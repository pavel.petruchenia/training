public class TextBoxPage extends Page {

    public TextBoxPage() {
        super();
    }

    public void pressSubmitButton() {
        getElement(Locators.TextBoxPageElements.submitButton).click();
    }

    public String getMainHeaderText() {
        return getElement(Locators.PageElements.textHeader).getText();
    }
    public TextBoxPage enterFullName(String fullName) {
        getElement(Locators.TextBoxPageElements.fullName).sendKeys(fullName);
        return this;
    }

    public TextBoxPage enterEmail(String email) {
        getElement(Locators.TextBoxPageElements.email).sendKeys(email);
        return this;
    }

    public TextBoxPage enterCurrentAddress(String currentAddress) {
        getElement(Locators.TextBoxPageElements.currentAddress).sendKeys(currentAddress);
        return this;
    }

    public TextBoxPage enterPermanentAddress(String permanentAddress) {
        getElement(Locators.TextBoxPageElements.permanentAddress).sendKeys(permanentAddress);
        return this;
    }

    public TextBoxPage fillTextBoxForm(String fullName, String email, String currentAddress, String permanentAddress) {
        this.enterFullName(fullName);
        this.enterEmail(email);
        this.enterCurrentAddress(currentAddress);
        this.enterPermanentAddress(permanentAddress);
        this.pressSubmitButton();
        return new TextBoxPage();
    }
    public String resultTextBox() {
        return getElement(Locators.TextBoxPageElements.actualResult).getText();
    }

}
